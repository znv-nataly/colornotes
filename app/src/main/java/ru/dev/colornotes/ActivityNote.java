package ru.dev.colornotes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

public class ActivityNote extends AppCompatActivity implements DialogColors.OnDialogColorsResultListener {

    private final int MENU_CHANGE_COLOR_ID = 1;
    private final int MENU_DELETE_ID = 2;
    private final int MENU_SAVE_ID = 3;

    private final String KEY_ID_NOTE = "id_note";

    private long idNote;
    private String oldTextNote;
    private ScrollView svNote;
    private EditText etNote;

    private DialogColors dialogColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("myLog", "create");
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_note);

            try {
                assert getSupportActionBar() != null;
                // don't show header of window
                getSupportActionBar().setDisplayShowTitleEnabled(false);

                // show icons on top panel
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setLogo(R.mipmap.ic_color_notes);
                getSupportActionBar().setDisplayUseLogoEnabled(true);

                // show arrow "<-" (home) on top panel
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorActionBar, R.drawable.ic_error, e);
                finish();
            }

            try {
                // open connection with database
                DB.getInstance(this);
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorDbCreate, R.drawable.ic_error, e);
                finish();
            }

            try {
                svNote = (ScrollView) findViewById(R.id.svNote);
                etNote = (EditText) findViewById(R.id.etNote);

                // get data about note
                Intent intent = getIntent();
                idNote = intent.getLongExtra(DB.COLUMN_ID_NOTE, 0);
                oldTextNote = intent.getStringExtra(DB.COLUMN_TEXT_NOTE);
                etNote.setText(oldTextNote);
                int color = intent.getIntExtra(DB.COLUMN_COLOR, 0);

                // color of note
                setColorWindowNote(color);

                // set cursor at the end
                etNote.setSelection(etNote.getText().length());
            } catch (Exception e) {
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorFillData, R.drawable.ic_error, e);
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putLong(KEY_ID_NOTE, idNote);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            idNote = savedInstanceState.getLong(KEY_ID_NOTE);
        }
    }

    /**
     * Show keyboard when there is click on the input field and  on free space of note
     * @param v element which click on
     */
    public void showSoftInput(View v) {

        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(etNote, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        try {
            menu.add(0, MENU_DELETE_ID, 0, R.string.menu_delete)
                    .setIcon(R.drawable.ic_trash)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(MENU_DELETE_ID).setVisible(idNote > 0);
            menu.add(0, MENU_CHANGE_COLOR_ID, 0, R.string.menu_change_color)
                    .setIcon(R.drawable.ic_colors)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.add(0, MENU_SAVE_ID, 0, R.string.menu_save)
                    .setIcon(R.drawable.ic_save)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            return super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    startActivity(new Intent(getApplicationContext(), ActivityMain.class));
                    break;
                case MENU_CHANGE_COLOR_ID:
                    try {
                        if (dialogColors == null)
                            dialogColors = new DialogColors();
                        dialogColors.idNote = idNote;
                        dialogColors.show(getFragmentManager(), "dialogColors");
                    } catch (Exception e) {
                        AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorCreateDialogColor, R.drawable.ic_error, e);
                    }
                    break;
                case MENU_DELETE_ID:
                    DialogInterface.OnClickListener deleteNote = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                // remove note
                                try {
                                    DB.getInstance(getApplicationContext()).deleteNote(idNote);
                                } catch (Exception e) {
                                    AlertDialogWindow.showMessage(ActivityNote.this, R.string.titleError, R.string.errorDelete, R.drawable.ic_error, e);
                                    return;
                                }
                                finish();
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    AlertDialogWindow.showConfirmMessage(this, R.string.titleConfirm, R.string.textConfirmDelete, R.drawable.ic_delete, R.string.btn_negative, R.string.btn_positive, deleteNote, null);
                    break;
                case MENU_SAVE_ID:
                    saveNote(true);
                    break;
            }
            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Save changes of note and finish work of current  activity
     * @param showToastResultOk show pop-up message if success saving
     * @return boolean
     */
    public boolean saveNote(boolean showToastResultOk) {

        try {
            if (idNote == 0 && !etNote.getText().toString().trim().equals("")) {
                // add new note
                try {
                    idNote = DB.getInstance(this).addNote(etNote.getText().toString(), getColorNote());
                    oldTextNote = etNote.getText().toString();
                    // update menu that view button for remove note
                    invalidateOptionsMenu();
                    if (showToastResultOk) {
                        Toast.makeText(this, getResources().getString(R.string.successSave), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorAdd, R.drawable.ic_error, e);
                    return false;
                }
            } else if (idNote > 0 && !etNote.getText().toString().trim().equals("")) {
                // editing
                try {
                    DB.getInstance(this).changeNote(idNote, etNote.getText().toString(), getColorNote());
                    oldTextNote = etNote.getText().toString();
                    if (showToastResultOk) {
                        Toast.makeText(this, getResources().getString(R.string.successSave), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorEdit, R.drawable.ic_error, e);
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Implementation of method onSetColor of interface DialogColors.OnDialogColorsResultListener
     * for choose color
     * @param idNote ИД заметки
     * @param color цвет заметки
     */
    @Override
    public void onSetColor(long idNote, int color) {

        try {
            // change color of window
            setColorWindowNote(color);
            if (idNote > 0) {
                // save color note
                DB.getInstance(this).changeColorNote(idNote, color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        // handler of click button "Back"
        super.onBackPressed();
        saveNote(false);
    }

    @Override
    protected void onStart() {
        try {
            super.onStart();
            DB.getInstance(this);
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorUnknown, R.drawable.ic_error, e);
        }
    }

    @Override
    protected void onDestroy() {

        try {
            super.onDestroy();
            // close connection with database
            DB.getInstance(this).closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get color of note by background
     * @return int color
     */
    private int getColorNote() {

        return ((ColorDrawable)svNote.getBackground()).getColor();
    }

    /**
     * Окрашивания окна в цвет заметки
     * @param color цвет заметки
     */
    private void setColorWindowNote(int color) {

        try {
            // цвет StatusBar
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //    getWindow().setStatusBarColor(DialogColors.getColorDark(this, color));
            //}

            // цвет ActionBar
            //if (getSupportActionBar() != null) {
            //    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(DialogColors.getColorDark(this, color)));
            //}

            // цвет фона текста
            svNote.setBackgroundColor(color);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {

        super.onPause();

        // auto-save
        saveNote(false);
    }
}
