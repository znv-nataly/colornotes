package ru.dev.colornotes;

import android.appwidget.AppWidgetManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import java.io.Serializable;

/**
 * Class for work with database
 */
class DB implements Serializable {

    private static final String DB_NAME = "db_color_notes";
    private static final int DB_VERSION = 5;
    private static final String TABLE_NOTES = "notes";

    static final String COLUMN_ID_NOTE = "_id";
    static final String COLUMN_TEXT_NOTE = "text";
    static final String COLUMN_DATE_ADD = "date_add";
    static final String COLUMN_COLOR = "color";
    static final String COLUMN_ID_WIDGET = "id_widget";

    // sql-code for creation table TABLE_NOTES
    private static final String  CREATE_TABLE_NOTES =
            "CREATE TABLE " + TABLE_NOTES + " (" +
                    COLUMN_ID_NOTE + " integer primary key, " +
                    COLUMN_TEXT_NOTE + " text," +
                COLUMN_DATE_ADD + " integer DEFAULT 0, " +
                COLUMN_COLOR + " integer DEFAULT 0, " +
                COLUMN_ID_WIDGET + " integer DEFAULT 0 " +
            ");";

    private static DBHelper dbHelper;
    private static SQLiteDatabase db;

    private static Context context;

    private DB(Context context) {
        this.context = context;
    }

    private static DB instance;

    static DB getInstance(Context context) {

        if (instance == null) {
            instance = new DB(context);
        }
        checkConnection();
        return instance;
    }

    /**
     * Open connection with database
     */
    private static void openConnection() {

        if (db != null && db.isOpen()) {
            return;
        }
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        db = dbHelper.getWritableDatabase();
    }

    /**
     * Close connection with database
     */
    void closeConnection() {

        try {
            if (dbHelper != null) dbHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check connection with DB. If it's closed then open
     */
    private static void checkConnection() {

        if (db == null || !db.isOpen()) {
            openConnection();
        }
    }

    /**
     * Add note in table TABLE_NOTES
     * @param text text of note
     * @param color color of note
     * @return long ID of inserted note
     * @throws Exception
     */
    long addNote(String text, int color) throws Exception {

        int time = (int)((new java.util.Date()).getTime()/1000);
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TEXT_NOTE, text);
        cv.put(COLUMN_DATE_ADD, time);
        cv.put(COLUMN_COLOR, color);

        checkConnection();
        long id = db.insert(TABLE_NOTES, null, cv);
        if (id <= 0) {
            throw new Exception("Error insert data.");
        }
        return id;
    }

    /**
     * Delete note from table TABLE_NOTES
     * @param id ID of note
     */
    void deleteNote(long id) {

        checkConnection();
        db.delete(TABLE_NOTES, COLUMN_ID_NOTE + " = '" + id + "'", null);
    }

    /**
     * Multiple removal notes
     * @param ids array of IDs of notes
     */
    void multiDeleteNotes(Object[] ids) {

        checkConnection();
        db.delete(TABLE_NOTES, COLUMN_ID_NOTE + " IN (" + TextUtils.join(",", ids) + ")", null);
    }

    /**
     * Change note
     * @param id ID of note
     * @param text text of note
     * @param color color of note
     */
    void changeNote(long id, String text, int color) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TEXT_NOTE, text);
        cv.put(COLUMN_COLOR, color);

        checkConnection();
        db.update(TABLE_NOTES, cv, COLUMN_ID_NOTE + " = '" + id + "'", null);

        // update widget, if there is so widget for edited note
        WidgetNote.updateWidgetByIdNote(context, id);
    }

    /**
     * Change color of note
     * @param id ID of note
     * @param color color of note
     */
    void changeColorNote(long id, int color) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_COLOR, color);

        checkConnection();
        db.update(TABLE_NOTES, cv, COLUMN_ID_NOTE + " = '" + id + "'", null);

        // update widget, if there is so widget for edited note
        WidgetNote.updateWidgetByIdNote(context, id);
    }

    /**
     * Get all notes
     * @return Cursor
     */
    Cursor getAllNotes() {

        String columns[] = new String[] { COLUMN_ID_NOTE, COLUMN_TEXT_NOTE, COLUMN_COLOR, COLUMN_DATE_ADD + " as " + COLUMN_DATE_ADD + "_int",
                "strftime('%d-%m-%Y %H:%M', " + COLUMN_DATE_ADD + ", 'unixepoch', 'localtime') as " + COLUMN_DATE_ADD, COLUMN_ID_WIDGET };

        checkConnection();
        return db.query(TABLE_NOTES, columns, null, null, null, null, COLUMN_DATE_ADD + "_int desc");
    }

    /**
     * Get notes without widget
     * @return Cursor
     */
    Cursor getNotesNotWidget() {

        String columns[] = new String[] { COLUMN_ID_NOTE, COLUMN_TEXT_NOTE, COLUMN_COLOR, COLUMN_ID_WIDGET,
                "strftime('%d-%m-%Y %H:%M', " + COLUMN_DATE_ADD + ", 'unixepoch', 'localtime') as " + COLUMN_DATE_ADD };

        checkConnection();
        return db.query(TABLE_NOTES, columns, COLUMN_ID_WIDGET + " = " + AppWidgetManager.INVALID_APPWIDGET_ID, null, null, null, null);
    }

    /**
     * Get data of note by ID of widget which binding to its
     * @param idWidget ID of widget
     * @return Cursor
     */
    Cursor getNoteInfoByIdWidget(int idWidget) {

        String columns[] = new String[] { COLUMN_ID_NOTE, COLUMN_TEXT_NOTE, COLUMN_COLOR, COLUMN_ID_WIDGET,
                "strftime('%d-%m-%Y %H:%M', " + COLUMN_DATE_ADD + ", 'unixepoch', 'localtime') as " + COLUMN_DATE_ADD };

        checkConnection();
        return db.query(TABLE_NOTES, columns, COLUMN_ID_WIDGET + " = " + idWidget, null, null, null, null, "1");
    }

    /**
     * Get data of note by ID
     * @param id ID of note
     * @return Cursor
     */
    Cursor getNoteInfoById(long id) {

        String columns[] = new String[] { COLUMN_ID_NOTE, COLUMN_TEXT_NOTE, COLUMN_COLOR, COLUMN_ID_WIDGET,
                "strftime('%d-%m-%Y %H:%M', " + COLUMN_DATE_ADD + ", 'unixepoch', 'localtime') as " + COLUMN_DATE_ADD };

        checkConnection();
        return db.query(TABLE_NOTES, columns, COLUMN_ID_NOTE + " = " + id, null, null, null, null, "1");
    }

    /**
     * Set ID widget for note
     * @param id ID of note
     * @param idWidget ID of widget
     */
    void setIdWidget(long id, int idWidget) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID_WIDGET, idWidget);

        checkConnection();

        db.update(TABLE_NOTES, cv, COLUMN_ID_NOTE + " = '" + id + "'", null);
    }

    /**
     * Remove binding widget to anything note
     * @param idWidget ИД виджета
     */
    void unsetWidget(int idWidget) {

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID_WIDGET, AppWidgetManager.INVALID_APPWIDGET_ID);

        checkConnection();

        try {
            db.update(TABLE_NOTES, cv, COLUMN_ID_WIDGET + " = " + idWidget, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Class for create and management of DB
    private static class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, dbName, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            // crete table TABLE_NOTES
            db.execSQL(CREATE_TABLE_NOTES);

            // create first note
            ContentValues cv = new ContentValues();
            int time = (int)((new java.util.Date()).getTime()/1000);

            // color of note set randomly
            // generate randomly number from 0 to quantity of colors
            int numColor = ActivityMain.getRandom().nextInt(DialogColors.getArrayColors().length);
            int color = DialogColors.getArrayColors()[numColor];

            cv.put(DB.COLUMN_TEXT_NOTE, context.getResources().getString(R.string.textFirstNote));
            cv.put(DB.COLUMN_COLOR, ContextCompat.getColor(context, color));
            cv.put(DB.COLUMN_DATE_ADD, time);
            db.insert(TABLE_NOTES, null, cv);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
