package ru.dev.colornotes;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ActivityWidgetConfig extends AppCompatActivity {

    private final int MENU_ADD_ID = 1;

    private int idWidget = AppWidgetManager.INVALID_APPWIDGET_ID;
    private Intent resultIntent;

    private String[] from = new String[] { DB.COLUMN_TEXT_NOTE, DB.COLUMN_DATE_ADD, DB.COLUMN_COLOR };
    private int[] to = new int[] { R.id.tvItem, R.id.tvDateAdd, R.id.llItem };

    private ListView lvNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        try {
            // get ID widget
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                idWidget = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            }

            // check
            if (idWidget == AppWidgetManager.INVALID_APPWIDGET_ID) {
                finish();
            }

            // creation intent for answer
            resultIntent = new Intent();
            resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, idWidget);

            // negative answer. If user click "Back", system gets answer that creation of widget is unnecessary
            setResult(RESULT_CANCELED, resultIntent);

            setContentView(R.layout.activity_main);
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, 0, R.drawable.ic_error, e);
            return;
        }

        try {
            lvNotes = ((ListView) findViewById(R.id.lvNotes));
            lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        // save binding between note and widget
                        DB.getInstance(getApplicationContext()).setIdWidget(id, idWidget);

                        // update widget
                        WidgetNote.updateWidgetByIdWidget(getApplicationContext(), idWidget);

                        setResult(RESULT_OK, resultIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();
                }
            });
            fillListView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Filling list of notes
     */
    private void fillListView() {

        try {
            // create adapter
            Cursor cursor = DB.getInstance(this).getNotesNotWidget();
            SimpleCursorAdapterListNotes adapter = new SimpleCursorAdapterListNotes(this, R.layout.item, cursor, from, to, 0, false);
            lvNotes.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorCursorLoaderManager, R.drawable.ic_error, e);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        try {
            // items of menu
            menu.add(0, MENU_ADD_ID, 0, R.string.menu_add)
                    .setIcon(R.drawable.ic_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            return super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == MENU_ADD_ID) {
            try {
                // add new note
                Intent intent = ActivityMain.getIntentForAddNote(this);
                startActivityForResult(intent, 0);
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorOpenActivity, R.drawable.ic_error, e);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // update list of notes
        fillListView();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
