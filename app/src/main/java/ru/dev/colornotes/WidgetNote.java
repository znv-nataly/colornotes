package ru.dev.colornotes;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Widget for note
 */
public class WidgetNote extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        // update all widget
        for (int id : appWidgetIds) {
            updateWidgetByIdWidget(context, id);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        try {
            super.onDeleted(context, appWidgetIds);

            for (int idWidget : appWidgetIds) {
                // remove binding between note and widget
                DB.getInstance(context).unsetWidget(idWidget);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update appearance of widget
     * @param context context
     * @param idNote ID of note
     * @param text text
     * @param color color of note
     * @param idWidget ID widget
     */
    public static void updateWidget(Context context, long idNote, String text, int color, int idWidget) {

        // set data in text fields
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_note);
        remoteViews.setTextViewText(R.id.tvItem, text);
        remoteViews.setInt(R.id.colorNote, "setBackgroundColor", color);

        int darkColor = DialogColors.getColorDark(context, color);
        remoteViews.setInt(R.id.darkColorNote, "setBackgroundColor", darkColor);

        Log.d("myLog", "updateWidget");
        // if we clicked on note then open activity for editing
        Intent intent = new Intent(context, ActivityNote.class);
        intent.putExtra(DB.COLUMN_ID_NOTE, idNote);
        intent.putExtra(DB.COLUMN_TEXT_NOTE, text);
        intent.putExtra(DB.COLUMN_COLOR, color);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, idWidget, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.colorNote, pendingIntent);

        // update widget
        AppWidgetManager.getInstance(context).updateAppWidget(idWidget, remoteViews);
    }

    /**
     * Update appearance of widget
     * @param context context
     * @param idWidget ID widget
     */
    public static void updateWidgetByIdWidget(Context context, int idWidget) {
        try {
            Cursor cursor = DB.getInstance(context).getNoteInfoByIdWidget(idWidget);
            if (cursor.getCount() == 0) {
                return;
            }

            cursor.moveToFirst();
            long idNote = cursor.getLong(cursor.getColumnIndex(DB.COLUMN_ID_NOTE));
            String text = cursor.getString(cursor.getColumnIndex(DB.COLUMN_TEXT_NOTE));
            int color = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_COLOR));

            updateWidget(context, idNote, text, color, idWidget);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update widget by binding ID note
     * @param context context
     * @param idNote ID note
     */
    public static void updateWidgetByIdNote(Context context, long idNote) {
        try {
            Cursor cursor = DB.getInstance(context).getNoteInfoById(idNote);
            if (cursor.getCount() == 0) {
                return;
            }

            cursor.moveToFirst();
            int idWidget = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_ID_WIDGET));
            String text = cursor.getString(cursor.getColumnIndex(DB.COLUMN_TEXT_NOTE));
            int color = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_COLOR));

            if (idWidget == AppWidgetManager.INVALID_APPWIDGET_ID) {
                return;
            }

            updateWidget(context, idNote, text, color, idWidget);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
