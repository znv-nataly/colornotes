package ru.dev.colornotes;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Random;


public class ActivityMain extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DialogColors.OnDialogColorsResultListener {

    private SimpleCursorAdapterListNotes adapter;
    private final int ID_LOADER = 0;
    private final int REQUEST_CODE_ACTIVITY_NOTE = 1;

    private final int MENU_ADD_ID = 1;
    private final int MENU_DELETE_ID = 2;

    private final int CM_DELETE_ID = 1;
    private final int CM_CHANGE_COLOR_ID = 2;

    private DialogColors dialogColors;
    private static Random random;

    private ListView lvNotes;
    private int countItems;

    public static Random getRandom() {

        random = (random == null) ? new Random(System.currentTimeMillis()) : random;
        return random;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            try {
                assert getSupportActionBar() != null;
                // don't show header of window (activity)
                getSupportActionBar().setDisplayShowTitleEnabled(false);

                // don't show icons on top panel
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setLogo(R.mipmap.ic_color_notes);
                getSupportActionBar().setDisplayUseLogoEnabled(true);

                // show arrow "<-" (home) on top panel
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorActionBar, R.drawable.ic_error, e);
                return;
            }

            try {
                // open connection with database
                DB.getInstance(this);
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorDbCreate, R.drawable.ic_error, e);
                finish();
            }

            try {
                // create adapter
                String[] from = new String[]{DB.COLUMN_TEXT_NOTE, DB.COLUMN_DATE_ADD, DB.COLUMN_COLOR };
                int[] to = new int[]{R.id.tvItem, R.id.tvDateAdd, R.id.llItem };

                adapter = new SimpleCursorAdapterListNotes(this, R.layout.item, null, from, to, 0, false);

                lvNotes = ((ListView) findViewById(R.id.lvNotes));
                lvNotes.setAdapter(adapter);

                // create loader for reading data
                getSupportLoaderManager().initLoader(ID_LOADER, null, this);
                lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        openNoteForEdit(id, view);
                    }
                });

                // context menu
                registerForContextMenu(lvNotes);

                random = new Random(System.currentTimeMillis());
            } catch (Exception e) {
                e.printStackTrace();
                AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorCursorLoaderManager, R.drawable.ic_error, e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        try {
            // items of menu of app
            menu.add(0, MENU_DELETE_ID, 0, R.string.menu_delete)
                    .setIcon(R.drawable.ic_trash)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(MENU_DELETE_ID).setVisible(lvNotes.getCount() > 0);
            menu.add(0, MENU_ADD_ID, 0, R.string.menu_add)
                    .setIcon(R.drawable.ic_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            return super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {
            // choose exactly item od menu
            Intent intent;

            if (item.getItemId() == MENU_ADD_ID) {
                try {
                    intent = getIntentForAddNote(this);
                    startActivityForResult(intent, REQUEST_CODE_ACTIVITY_NOTE);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorOpenActivity, R.drawable.ic_error, e);
                }
            } else if (item.getItemId() == MENU_DELETE_ID) {
                try {
                    intent = new Intent(this, ActivityMultiDeleteNotes.class);
                    startActivityForResult(intent, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorOpenActivity, R.drawable.ic_error, e);
                }
            } else if (item.getItemId() == android.R.id.home) {
                finish();
            }
            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Creation of object type of Intent for start activity for add note
     * @param context context
     * @return Intent
     */
    public static Intent getIntentForAddNote(Context context) {

        Intent intent = new Intent(context, ActivityNote.class);

        intent.putExtra(DB.COLUMN_ID_NOTE, 0);
        intent.putExtra(DB.COLUMN_TEXT_NOTE, "");

        // generate random color for created note
        // generate random number from 0 to quantity of colors
        int numColor = getRandom().nextInt(DialogColors.getArrayColors().length);
        int color = DialogColors.getArrayColors()[numColor];
        intent.putExtra(DB.COLUMN_COLOR, ContextCompat.getColor(context, color));

        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            // reload list of notes
            getSupportLoaderManager().getLoader(ID_LOADER).forceLoad();
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorUnknown, R.drawable.ic_error, e);
        }
    }

    @Override
    protected void onDestroy() {

        try {
            super.onDestroy();
            DB.getInstance(this).closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {

        try {
            super.onStart();
            DB.getInstance(this);
            // reload list of notes
            getSupportLoaderManager().getLoader(ID_LOADER).forceLoad();
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorUnknown, R.drawable.ic_error, e);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ListViewNotesCursorLoader(this);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        int count = data.getCount();
        if ((countItems == 0 && count > 0) || (countItems > 0 && count == 0)) {
            // обновляем меню
            invalidateOptionsMenu();
        }
        countItems = count;
        adapter.swapCursor(data);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        try {
            menu.add(0, CM_CHANGE_COLOR_ID, 0, R.string.context_menu_change_color);
            menu.add(0, CM_DELETE_ID, 0, R.string.context_menu_delete);
            super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        try {
            final AdapterView.AdapterContextMenuInfo cmInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            if (item.getItemId() == CM_DELETE_ID) {

                Cursor cursor = DB.getInstance(this).getNoteInfoById(cmInfo.id);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    int idWidget = cursor.getInt(cursor.getColumnIndex(DB.COLUMN_ID_WIDGET));
                    if (idWidget > AppWidgetManager.INVALID_APPWIDGET_ID) {
                        // check existence the widget
                        int[] appWidgetIds = AppWidgetManager.getInstance(getApplicationContext()).getAppWidgetIds(new ComponentName(this, WidgetNote.class));
                        for (int id : appWidgetIds) {
                            if (id == idWidget) {
                                AlertDialogWindow.showMessage(this, R.string.titleMessage, R.string.banDeleteNote, R.drawable.ic_info, null);
                                return true;
                            }
                        }
                        // there isn't the widget but there is connection with its. Remove this connection
                        DB.getInstance(this).unsetWidget(idWidget);
                    }
                }

                DialogInterface.OnClickListener deleteNote = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            DB.getInstance(getApplicationContext()).deleteNote(cmInfo.id);
                            getSupportLoaderManager().getLoader(ID_LOADER).forceLoad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                AlertDialogWindow.showConfirmMessage(this, R.string.titleConfirm, R.string.textConfirmDelete, R.drawable.ic_delete, R.string.btn_negative, R.string.btn_positive, deleteNote, null);
            } else if (item.getItemId() == CM_CHANGE_COLOR_ID) {
                try {
                    if (dialogColors == null) dialogColors = new DialogColors();
                    dialogColors.idNote = cmInfo.id;
                    dialogColors.show(getFragmentManager(), "dialogColors");
                } catch (Exception e) {
                    AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorCreateDialogColor, R.drawable.ic_error, e);
                }
            }
            return super.onContextItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Implementation of method onSetColor of interface DialogColors.OnDialogColorsResultListener
     * for choose color
     * @param idNote ИД заметки
     * @param color цвет заметки
     */
    @Override
    public void onSetColor(long idNote, int color) {

        try {
            DB.getInstance(this).changeColorNote(idNote, color);
            getSupportLoaderManager().getLoader(ID_LOADER).forceLoad();
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorChangeColor, R.drawable.ic_error, e);
        }
    }

    /**
     * Open window for editing note
     * @param id id notes
     * @param view element of list notes
     */
    public void openNoteForEdit(long id, View view) {

        try {
            int color = ((ColorDrawable)view.getBackground()).getColor();
            String text = ((TextView)view.findViewById(R.id.tvItem)).getText().toString();

            // open note for editing
            Intent intent = new Intent(getApplicationContext(), ActivityNote.class);
            intent.putExtra(DB.COLUMN_ID_NOTE, id);
            intent.putExtra(DB.COLUMN_TEXT_NOTE, text);
            intent.putExtra(DB.COLUMN_COLOR, color);

            startActivityForResult(intent, REQUEST_CODE_ACTIVITY_NOTE);
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialogWindow.showMessage(this, R.string.titleError, R.string.errorGetData, R.drawable.ic_error, e);
        }
    }

    // View and update data in notes list
    static class ListViewNotesCursorLoader extends CursorLoader {

        ListViewNotesCursorLoader(Context context) {
            super(context);
        }

        @Override
        protected Cursor onLoadInBackground() {

            try {
                return DB.getInstance(super.getContext()).getAllNotes();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
