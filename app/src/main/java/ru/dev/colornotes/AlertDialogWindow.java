package ru.dev.colornotes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Dialog window for output message
 */

class AlertDialogWindow {

    private static AlertDialog.Builder alertDialogBuilderMessage;
    private static AlertDialog.Builder alertDialogBuilderConfirm;

    /**
     * Show dialog window with text message and 1 button
     * @param context context
     * @param titleResource header
     * @param messageResource message
     * @param icon icon
     * @param exception exception
     */
    static void showMessage(Context context, int titleResource, int messageResource, int icon, Exception exception) {

        try {
            if (alertDialogBuilderMessage == null || alertDialogBuilderMessage.getContext() != context) {
                alertDialogBuilderMessage = new AlertDialog.Builder(context);
                alertDialogBuilderMessage.setCancelable(false)
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
            }

            alertDialogBuilderMessage.setTitle(context.getResources().getString(titleResource))
                    .setIcon(icon)
                    .setMessage(context.getResources().getString(messageResource) + ". " + (exception == null ? "" : exception.getMessage()));
            alertDialogBuilderMessage.show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.errorOpenAlertDialog, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Show dialog window for confirm any actions
     * @param context context
     * @param titleResource header
     * @param messageResource message
     * @param icon icon
     * @param btnNegative button "No"
     * @param btnPositive button "Yes"
     * @param onClickPositiveButton handler click button "Yes"
     */
    static void showConfirmMessage(Context context,
                                          int titleResource,
                                          int messageResource,
                                          int icon,
                                          int btnNegative,
                                          int btnPositive,
                                          @NonNull DialogInterface.OnClickListener onClickPositiveButton,
                                          @Nullable DialogInterface.OnClickListener onClickNegativeButton) {
        try {
            if (alertDialogBuilderConfirm == null || alertDialogBuilderConfirm.getContext() != context) {
                alertDialogBuilderConfirm = new AlertDialog.Builder(context);
            }

            alertDialogBuilderConfirm.setTitle(titleResource)
                    .setIcon(icon)
                    .setMessage(context.getResources().getString(messageResource));
            if (onClickNegativeButton == null) {
                alertDialogBuilderConfirm.setNegativeButton(btnNegative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
            } else {
                alertDialogBuilderConfirm.setNegativeButton(btnNegative, onClickNegativeButton);
            }
            alertDialogBuilderConfirm.setPositiveButton(btnPositive, onClickPositiveButton);
            alertDialogBuilderConfirm.show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.errorOpenAlertDialog, Toast.LENGTH_LONG).show();
        }
    }
}
